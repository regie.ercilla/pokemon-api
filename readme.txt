

Requirements:


Assume that you just started your Pokemon journey to becoming a Pokemon master. Prof. Oak asked you to keep a track of the sound which your Pokemon makes 
with the timestamp as it will help him in his research. Being a backend programmer you decided to automate the tracking task and provide data to Prof. Oak 
in real time by building an HTTP API. The API can be used to save the value in Pokedex and query the value from the same.The API needs to able to:

1.	Accept a Pokemon Name(String) and Sound(String) {"pikachu" : "pika-pika"} and store them. If an existing key is sent, the value should be updated
2.	Accept a Pokemon Name(String) and return the corresponding latest sound it made
3.	When an existing Pokemon Name(String) and a timestamp is provided, return the sound the pokemon made at the given timestamp

Timestamp:  https://www.npmjs.com/package/set-timezone-time

________________________________________

Method: POST
Endpoint: /add
Body:(JSON): {"pikachu" : "pika-pika"}
Time: 06:00 pm
Response: 
//Where time is timestamp of the post request (06:00 pm) .
{
	"pokemon":"pikachu", 
	"sound":"pika-pika", 
	"timestamp": time 
} 
________________________________________
Method: GET
Endpoint: /pokemon/pokemon_name (for above example it will be : /pokemon/pikachu
Response: 
{
	"pikachu": "pika-pika"
}
________________________________________
Method: POST
Endpoint: /add
Body: JSON: {"pikachu" : "chuuuu"}
Time: 06:05 pm
Response: 
{
	"pokemon":"pikachu", 
	"sound":"chuuuu", 
	"timestamp": time 
}  //Where time is timestamp of the new value (06:05 pm) .
________________________________________
Method: GET
Endpoint: /pokemon/pokemon_name (for above example it will be : /pokemon/pikachu
Response: 
{
	"pikachu": "chuuuu"
}
________________________________________
Method: GET
Endpoint: /pokemon/pikachu?timestamp=1523426580 [6:03pm] // notice that the time here is not exactly 6:00 pm
Response: 
{
	"pikachu": "pika-pika" 
} // still return sound 1 , because sound 2 was only added at 6:05 pm




=======================================================================================

Running tests:


1.  To retrieve data, just do a GET to this url in your Postman:  localhost:3000/pokemons


2.  To store data, just do a POST in Postman:  localhost:3000/pokemons
	The timestamp is auto generated.
	
	{
		"name": "Evie",
		"sound": "eeeeeeviiiieeee"
	}