
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Pokedex');

var time = require('time')(Date);

//var d = new Date();
//d.setTimezone('UTC');
//var now = d.getTime.getUTCDate;



var PokedexSchema = new mongoose.Schema({
    name: String,
    sound: String,
    timestamp: { type: Date, default: Date.now }
});

var pokemonModel = mongoose.model('Pokemons', PokedexSchema);
// pokemonModel.create({
//     name: 'pikachu',
//     sound: 'waaaaaaa',
//     timestamp: new Date
// }).then(function(err, pokemon){
//     console.log(err, pokemon);
// });


module.exports = pokemonModel;