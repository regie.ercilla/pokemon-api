var Pokedex = require('./pokedexModel');



exports.create = function (req, res){
    
    var pokemons = new Pokedex(req.body);
    console.log(pokemons);
    pokemons.save();

    res.status(201).send(pokemons);
};


exports.findAll = function (req, res) {

    var query = req.query;
    Pokedex.find(query, function(err, pokemons){
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            res.json(pokemons);
        }
    })

};


exports.findById = function (req, res) {

    Pokedex.findById(req.params.id, function(err, pokemon){
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            res.json(pokemon);
        }
    })

};

exports.findByName = function (req, res) {
    Pokedex.find(req.params.name, function(err, pokemon){
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            res.json(pokemon);
        }
    })
};

exports.update = function (req, res) {

};