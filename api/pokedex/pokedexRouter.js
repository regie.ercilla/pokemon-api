var _ = require('lodash');
var pokedexRouter = require('express').Router();

var pokedexController = require('./pokedexController');

var pokemons = [];

// Get all pokemons
pokedexRouter.get('/', function(req, res){
    console.log('path : pokemons' + req.url);

    pokedexController.findAll(req, res);
});

pokedexRouter.post('/', function(req, res) {
    console.log('path : pokemons' + req.url);
    
    pokedexController.create(req, res);
});

// Get pokemon entries with name
// pokedexRouter.get('/:name', function(req, res){
//     console.log('path : pokemons' + req.url);
//     pokedexController.findByName(req, res);
// });

// Get pokemon entries with id
// pokedexRouter.get('/:id', function(req, res){
//     console.log('path : pokemons' + req.url);
//     pokedexController.findById(req, res);
// });


module.exports = pokedexRouter;