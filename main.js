var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');
var result = express.static('client');
var port = process.env.port || 3000;

var app = express();
var pokedexRouter = require('./api/pokedex/pokedexRouter');
var tempModel = require('./api/pokedex/pokedexModel');

app.use(result);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/pokemons', pokedexRouter );


app.get('/tests', function(req, res){
    console.log('path : ' + req.url);
    var query = req.query;
    var testJSONResponse = tempModel.find(query, function(err, pokemons){
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            res.json(pokemons);
        }
    })

    //res.send('APIs available');
});



app.use(function(err, req, res, next){
    if (err) {
        console.log(err);
        res.status(500).send(err);
    }
});

app.listen(port, function(){
    console.log('Running on port:' +port);
});